#!/usr/bin/env bash
#
virtualenv env -p python3.7
source env/bin/activate
pip install --upgrade pip
pip install -r scripts/requirements.txt
