#!/usr/bin/env bash
#
source env/bin/activate
#
pytest mundane --cov=mundane --cov-report term-missing
#pytest --pylint bets/
flake8 mundane
#
